#!/bin/bash
echo "begin search files in $1"
#files=$(find $1 -iname "*.jpg" | xargs)
mw=1920
mh=1920
find $1 -iname "*.jpg" | while read f
do
   iw=$(identify -format "%w" "$f")
   ih=$(identify -format "%h" "$f")
   if [ "$iw" -gt "$mw" ] || [ "$ih" -gt "$mh" ]
   then
        mogrify -resize ''"$mw"x"$mh"\>'' -verbose $f
    else
        echo "skip $f"
    fi
done